package edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.BARCODE;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.NAME;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.NCC_ID;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.PHONE;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.TABLE_NAME;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.NonNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import edu.ncc.nest.nestapp.BuildConfig;
import java.text.DecimalFormat;

//import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.QuestionnaireHelper;

/**
 * GuestRegistrySource: Handles the insertion and removal of data into the GuestRegistry database
 * (See {@link GuestRegistryHelper}). Also has methods that allow for searching the database.
 */
public class GuestRegistrySource {

    private static final String USERNAME = BuildConfig.GUEST_REGISTRY_DB_USERNAME; // Username for the database
    private static final String PASSWORD = BuildConfig.GUEST_REGISTRY_DB_PASSWORD; // Password for the database
    private static final String DRIVER = "jdbc:mysql://";                          // Driver that is used to connect to the database
    private static final String DB_NAME = "GuestRegistry";                         // Name of the remote DB
    private static final String SERVER = "stargate.ncc.edu:3306/";                      // Name of the server that the DB is hosted on
    private static final String TAG = "GuestRegistrySource";                      // Tag for logging
    private final GuestRegistryHelper registryHelper;
    private final SQLiteDatabase database;
    private final String URI = DRIVER + SERVER + DB_NAME;                          // URI to communicate with the database

    /**
     * Default Constructor --
     *
     * @param context
     */
    public GuestRegistrySource(Context context) {

        registryHelper = new GuestRegistryHelper(context);

        // Open a database that will be used for reading and writing
        database = registryHelper.getWritableDatabase();

    }

    /**
     * close --
     * Closes the database.
     */
    public void close() {

        database.close();

        registryHelper.close();

    }

    /**
     * insertData method --
     * Takes the data passed as arguments and inserts it into the database appropriately.
     * If the data is inserted without issue, the value for the row ID will be returned,
     * and in turn the method will return a boolean value of true. If an issue does occur,
     * -1 will be returned and the method will return a boolean value of false.
     *
     * @param name            - the name of the guest
     * @param phone           - the phone number of the guest
     * @param nccID           - the NCC ID of the guest
     * @param address         - the address of the guest
     * @param city            - the city of the guests' address
     * @param zipcode         - the zip-code of the guests' address
     * @param state           - state that guest's address is belong to
     * @param affiliation     - affiliation of the guest
     * @param age             - age of the guest
     * @param gender          - gender of the guest
     * @param diet            - diet that guest follows
     * @param programs        - guest's programs
     * @param snap            - does the guest have SNAP / Food stamps
     * @param employment      - employment status of the guest
     * @param health          - what kind of insurance does the guest have
     * @param housing         - housing of the guest
     * @param income          - the income of the guest
     * @param householdNum    - the number of people in the guest's household
     * @param childcareStatus - childcare status of the guest
     * @param children1       - number of guest's children under the age of 1 year old
     * @param children5       - number of guest's children between the age of 13 months and 5 years old
     * @param children12      - number of guest's children between the age of 6 and 12 years old
     * @param children18      - number of the guest's children between the age of 13 and 18 years old
     * @param additionalInfo  - additional information provided by the guest
     * @param nameOfVolunteer - name of the volunteer
     * @param barcode         - the barcode that belongs to the guest
     * @return true if the data has been inserted without issue, false otherwise
     */
    public boolean insertData(String name, String phone, String nccID, String address, String city, String zipcode,
                              String state, String affiliation, String age, String gender, String diet, String programs,
                              String snap, String employment, String health, String housing, String income, String householdNum,
                              String childcareStatus, String children1, String children5, String children12, String children18,
                              String referralInfo, String additionalInfo, String nameOfVolunteer, String barcode) {
        Log.d("***", gender);
        // Creates an ExecutorService that uses a single worker thread operating off an unbounded queue.
        ExecutorService executor = Executors.newSingleThreadExecutor();

        AtomicBoolean didInsert = new AtomicBoolean(false);

        // Executes the thread that will insert the data into the remote db
        executor.execute(() -> {
            //Try to establish a connection to the db
            try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {

                //Compiles the sql statement
                PreparedStatement statement = connection.prepareStatement("INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

                // sets the values for the statement
                statement.setString(1, null);
                statement.setString(2, name);
                statement.setString(3, phone);
                statement.setString(4, nccID);
                statement.setString(5, "some date");
                statement.setString(6, address);
                statement.setString(7, city);
                statement.setString(8, zipcode);
                statement.setString(9, state);
                statement.setString(10, affiliation);
                statement.setString(11, age);
                statement.setString(12, gender);
                statement.setString(13, diet);
                statement.setString(14, programs);
                statement.setString(15, snap);
                statement.setString(16, employment);
                statement.setString(17, health);
                statement.setString(18, housing);
                statement.setString(19, income);
                statement.setString(20, householdNum);
                statement.setString(21, childcareStatus);
                statement.setString(22, children1);
                statement.setString(23, children5);
                statement.setString(24, children12);
                statement.setString(25, children18);
                statement.setString(26, referralInfo);
                statement.setString(27, additionalInfo);
                statement.setString(28, nameOfVolunteer);
                statement.setString(29, barcode);
                statement.setString(30, "some date");

                // Execute the statement
                statement.executeUpdate();
                didInsert.set(true);
            } catch (SQLException e) {
                Log.e(TAG, "Error inserting data into database", e);
            }
        });

        // Shuts down the executor service so that it no longer accepts new tasks
        executor.shutdown();


        // Insert method will return a negative 1 if there was an error with the insert
        return didInsert.get();

    }

    public int removeData() {
        return database.delete(TABLE_NAME, null, null);
    }

    /**
     * isRegistered - Takes 1 parameter
     * Determines whether or not a guest is currently registered with the provided barcode
     * 3
     *
     * @param barcode - The barcode to search the database for
     * @return Returns the name of the first guest who is registered in the database with the barcode
     * or null if there is no guest registered with that barcode
     */
    public String[] isRegistered(@NonNull String barcode) {

        String[] info = new String[2];

        // Creates an ExecutorService that uses a single worker thread operating off an unbounded queue.
        ExecutorService executor = Executors.newSingleThreadExecutor();
        AtomicReference<String> name = null;


        // Executes the thread that will check if the guest is registered in the remote database
        executor.execute(() -> {
            //Try to establish a connection to the database
            try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {
                // prepares the statement passing the ContentValues object
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + BARCODE + " = ?");

                // Set the barcode in the statement
                statement.setString(1, barcode);

                // Execute the statement
                ResultSet resultSet = statement.executeQuery();

                // If the result set is not empty, the guest is registered and return the name
                if (resultSet.next())
                    name.set(resultSet.getString(NAME));
            } catch (SQLException e) {
                Log.e(TAG, "Error selecting data into database", e);
            }
        });

        //Log.d(TAG, "isRegistered: " + name.get());
        return null; // placeholder to get the code to compile - not sure what this method should be returning
        //return name.get(); // old return statement which is a causing a syntax error
    }


    /**
     * doesExist method --
     * Determines if the user is trying to register with an NCC ID or phone number
     * that already used to register another account. Selects the phone and nccID columns from
     * the database, and checks if phoneNum or nccId already exist in one of the rows.
     *
     * @param phoneNum - phone number that user trying to register with
     * @param nccId    - NCC ID that user trying to register with
     * @return if phone number or NCC ID already exist in the database return true, otherwise false
     */
    public boolean doesExist(String phoneNum, String nccId) {
        // Creates an ExecutorService that uses a single worker thread operating off an unbounded queue.
        ExecutorService executor = Executors.newSingleThreadExecutor();

        AtomicBoolean doesExist = new AtomicBoolean(false);

        // Executes the thread that will insert the data into the remote db
        executor.execute(() -> {
            //Try to establish a connection to the db
            try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {
                // prepares the statement passing the ContentValues object
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + PHONE + " = ? OR " + NCC_ID + " = ?");

                // Set the barcode in the statement
                statement.setString(1, phoneNum);
                statement.setString(2, nccId);

                // Execute the statement
                ResultSet resultSet = statement.executeQuery();

                // If the result set is not empty, the guest is registered and return the name
                if (resultSet.next()) {
                    Log.d("TESTING THE DOES EXIST", "doesExist: " + resultSet.getString(PHONE) + " " + resultSet.getString(NCC_ID));
                    doesExist.set(true);
                }
            } catch (SQLException e) {
                Log.e(TAG, "Error selecting data from the database", e);
            }
        });

        // Determine if there is at least 1 guest registered with the phone number or NCC ID
        Log.d("TESTING THE DOES EXIST", "doesExist: " + doesExist.get());
        return doesExist.get();
    }

    @Override // Called by the garbage collector
    protected void finalize() throws Throwable {

        // Make sure we close the writable database before this object is finalized
        database.close();

        super.finalize();

    }
    /**
     * getTotalGuests method - Counts the total number of guests registered with the NEST.
     * @return - THe total number of guests registered with the NEST.
     */
    public int getTotalGuests() {
        Cursor c = database.query(
                TABLE_NAME,
                new String[] {registryHelper._ID},
                null,
                null,
                null,
                null,
                null
        );
        int count = 0;
        while (c.moveToNext())
            count++;
        c.close();
        return count;
    }

    /**
     * getMonthlyGuests method - Counts the number of guests registered to the NEST in a given month
     * @param month - The month you wish to check
     * @param year - The year you wish to check
     * @return - The number of guests registered to the NEST in the given month
     */
    public int getMonthlyGuests(int month, int year) {
        DecimalFormat monthFormat = new DecimalFormat("00");
        String queryArgument = monthFormat.format(month) + "__" + year + "%";
        Cursor c = database.query(
                TABLE_NAME,
                new String[] {registryHelper._ID},
                registryHelper.DATE + " LIKE ? ",
                new String[] {queryArgument},
                null,
                null,
                null
        );
        int count = 0;
        while (c.moveToNext())
            count++;
        c.close();
        return count;
    }



}

